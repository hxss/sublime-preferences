import sublime
import sublime_plugin

import subprocess
import threading

class ClipFile(sublime_plugin.WindowCommand):
	cmd = "/home/hxss/mega/dev/c/clipfile/clipfile"
	methods = {
		"copy": "--copy",
		"cut": "--cut",
		"paste": "--paste",
	}

	def clipFile(self, method, paths):
		cmd = self.cmd + ' ' \
			+ method + ' ' \
			+ ' '.join(("'" + path + "'" for path in paths))

		threading.Thread(target=self.call, args=(cmd,)).start()

	def paste(self, path):
		cmd = self.cmd + ' ' \
			+ self.methods['paste'] + ' ' \
			+ path

		threading.Thread(target=self.call, args=(cmd,)).start()

	def call(self, cmd):
		subprocess.call(cmd, shell=True)

	def check(self):
		cmd = self.cmd + ' --check'

		r = subprocess.check_output(cmd,
			shell=True,
			universal_newlines=True,
			timeout=1
		)
		self.state = r.strip()
		pprint(self.state)
		pprint(self.state == '1')

		return self.state


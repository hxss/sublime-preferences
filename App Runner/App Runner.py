import sublime
import sublime_plugin

import collections
import os.path
import subprocess
import threading

from pprint import pprint

class RunApp(sublime_plugin.WindowCommand):
	window = None
	variables = {}
	modificators = {
		"file_base_name": lambda s, path: os.path.splitext(os.path.basename(path))[0],
		"file_extension": lambda s, path: os.path.splitext(os.path.basename(path))[1][1:],
		"file_name":      lambda s, path: os.path.basename(path),
		"file_path":      lambda s, path: os.path.dirname(path),
		"in_project":     lambda s, path: path.replace(s.variables['folder'] + '/', ''),
	}

	def __init__(self, window):
		self.window = window

		self.load_settings()
		self.settings.clear_on_change('reload')
		self.settings.add_on_change('reload', self.load_settings)

	def run(self, cmd, args):
		self.variables = self.window.extract_variables()
		args = self.flatten(args)

		if cmd in self.shortcuts:
			if 'args' in self.shortcuts[cmd]:
				args = self.shortcuts[cmd]['args'] + ' ' + args

			cmd = self.shortcuts[cmd]['cmd']

		threading.Thread(target=self.call, args=(cmd + ' ' + args,)).start()

	def call(self, cmd):
		subprocess.call(cmd, shell=True)

	def flatten(self, items):
		if type(items) is not str:
			if (type(items) is dict):
				_items = []
				for key in sorted(items.keys()):
					_items.append(
						self.apply_modificator(key, items[key])
					)

				items = _items

			items = [self.flatten(item) for item in items]
			items = ' '.join(
				sublime.expand_variables(items, self.variables)
			)

		return items

	def apply_modificator(self, modificator, path):
		if (modificator[0] == '$'
			and modificator[1:] in self.modificators
		):
			modificator = modificator[1:]
			if type(path) is not str:
				path = self.flatten(path)

			if (len(path) == 0):
				path = self.window.active_view().file_name()

			path = self.modificators[modificator](self, path)

		return path

	def load_settings(self):
		self.settings = sublime.load_settings('App Runner.sublime-settings')
		self.shortcuts = self.settings.get("shortcuts")

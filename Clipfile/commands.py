from .clipfile import ClipFile

class CopyFile(ClipFile):
	def run(self, paths):
		self.clipFile(self.methods['copy'], paths)

class CutFile(ClipFile):
	def run(self, paths):
		self.clipFile(self.methods['cut'], paths)

class PasteFile(ClipFile):
	def run(self, dirs):
		self.paste(dirs[0])

	def is_visible(self, dirs):
		return bool(len(dirs))
